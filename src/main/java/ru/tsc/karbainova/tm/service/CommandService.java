package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.ICommandRepository;
import ru.tsc.karbainova.tm.api.ICommandService;
import ru.tsc.karbainova.tm.model.Command;

public class CommandService implements ICommandService {
    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
