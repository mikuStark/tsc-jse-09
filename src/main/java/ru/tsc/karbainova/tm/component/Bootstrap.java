package ru.tsc.karbainova.tm.component;

import ru.tsc.karbainova.tm.api.ICommandController;
import ru.tsc.karbainova.tm.api.ICommandRepository;
import ru.tsc.karbainova.tm.api.ICommandService;
import ru.tsc.karbainova.tm.controller.CommandController;
import ru.tsc.karbainova.tm.repository.CommandRepository;
import ru.tsc.karbainova.tm.service.CommandService;

import java.util.Scanner;

import static ru.tsc.karbainova.tm.constant.ArgumentConst.*;
import static ru.tsc.karbainova.tm.constant.ArgumentConst.VERSION;
import static ru.tsc.karbainova.tm.constant.TerminalConst.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        commandController.displayWelcome();
        run(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseCommand(final String param) {
        switch (param) {
            case HELP:
                commandController.displayHelp();
                break;
            case ABOUT:
                commandController.displayAbout();
                break;
            case INFO:
                commandController.showInfo();
                break;
            case VERSION:
                commandController.displayVersion();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.displayErrorCommand();
        }
    }

    public void parseArg(final String param) {
        switch (param) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_EXIT:
                commandController.exit();
                break;
            default:
                commandController.displayErrorArg();
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        parseArg(param);
        System.exit(0);
    }
}
